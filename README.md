This repository is dedicated to preserving documentation related to the Heath/Zenith H8 and
H88/89/90 8-bit computers.

The website http://sebhc.lesbird.com is dedicated to 8-bit Heath/Zenith computer enthusiasts.
The Google group http://groups.google.com/group/sebhc is a good resource page.  Many of the
members have their own webpages.  Examples include Mark Garlanger
http://http://heathkit.garlanger.com/), Herb Johnson (http://www.retrotechnology.com/herbs_stuff/h8.html),
and Norberto Collado (http://http://koyado.com/Heathkit).

There are several ways to use this site:

1) If you just want selected manuals, navigate to them and download.

2) If you want a one-time copy of all manuals, navigate to
   https://bitbucket.org/HeathkitGuy/heathkitinformation/downloads/ and then click on
   "download repository".  That will create a zip of the current contents and allow
   you to download it.

2) If you want a copy of the repository that allows you to periodically pull updates when you want,
   you can "clone" the repository as follows:
   a) Navigate to the PARENT directory where you want the copy placed.
   b) From a command prompt issue:
        git clone https://HeathkitGuy@bitbucket.org/HeathkitGuy/heathkitinformation.git
   c) When you want an update, navigate to the actual directory (not its parent) and
      from a command prompt issue:
        git pull

4) If you want to know when content is added to the BitBucket repository navigate to:
   https://bitbucket.org/HeathkitGuy/heathkitinformation/src/master/ then click on ". . .",
   and finally click on "Watch" (or "Unwatch" if you want to stop watching).  From then on
   you will receive an email whenever a change is made.  The email will contain a brief
   description of the change.  If you are interested in the update, from a command prompt issue:
     git pull

Enjoy!

